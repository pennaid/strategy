//@version=3

strategy(title="Version 23", shorttitle="TRI", overlay=true)

//Functions
// WMA
wmalen = input(12, minval=1, title="WMA Length")
wmasrc = input(close, title="WMA Source")
wmaout = wma(wmasrc, wmalen)
//plot(wmaout, title="WMA", color=yellow)

// SMA
emalen = input(32, minval=1, title="EMALength")
emasrc = input(close, title="EMA Source")
emaout = sma(emasrc, emalen)
//plot(emaout, title="EMA", color=green)

// HMA
hulllength = input(9, minval=1, title="Hull Length")
hullsrc = input(close, title="HULL Source")
hullout = wma(2*wma(hullsrc, hulllength/2)-wma(hullsrc, hulllength), round(sqrt(hulllength)))
plot(hullout, title="HULL", color=red)

//BB
bblength = input(20, minval=1)
bbsrc = input(close, title="Source")
bbmult = input(2.0, minval=0.001, maxval=50)
bbbasis = sma(bbsrc, bblength)
bbdev = bbmult * stdev(bbsrc, bblength)
bbupper = bbbasis + bbdev
bblower = bbbasis - bbdev
//plot(bbbasis, color=black)
//bbp1 = plot(bbupper, color=blue)
//bbp2 = plot(bblower, color=blue)
//fill(bbp1, bbp2)

// RSI
rsisrc = close, lenrsi = 3, lenupdown = 2, lenroc = 100
updown(s) =>
    isEqual = s == s[1]
    isGrowing = s > s[1]
    ud = 0.0
    ud := isEqual ? 0 : isGrowing ? (nz(ud[1]) <= 0 ? 1 : nz(ud[1])+1) : (nz(ud[1]) >= 0 ? -1 : nz(ud[1])-1)
    ud
rsi = rsi(rsisrc, lenrsi)
updownrsi = rsi(updown(rsisrc), lenupdown)
percentrank = percentrank(roc(rsisrc, 1), lenroc)
crsi = avg(rsi, updownrsi, percentrank)
//plot(crsi)
//band1 = hline(70)
//band0 = hline(30)
//fill(band1, band0)

// ICHI
conversionPeriods = input(9, minval=1, title="Conversion Line Periods"),
basePeriods = input(26, minval=1, title="Base Line Periods")
laggingSpan2Periods = input(52, minval=1, title="Lagging Span 2 Periods"),
displacement = input(26, minval=1, title="Displacement")

donchian(len) => avg(lowest(len), highest(len))

conversionLine = donchian(conversionPeriods)
baseLine = donchian(basePeriods)
leadLine1 = avg(conversionLine, baseLine)
leadLine2 = donchian(laggingSpan2Periods)

//plot(conversionLine, color=blue, title="Conversion Line")
//plot(baseLine, color=orange, title="Base Line")
//plot(close, offset = -displacement, color=purple, title="Lagging Span")

//ichip1 = plot(leadLine1, offset = displacement, color=green,title="Lead 1")
//ichip2 = plot(leadLine2, offset = displacement, color=red, title="Lead 2")
//fill(ichip1, ichip2, color = leadLine1 > leadLine2 ? green : red)

// SAR

sarstart = input(0.02)
sarincrement = input(0.02)
sarmaximum = input(0.2)

psar = sar(sarstart, sarincrement, sarmaximum)

//closing variables

// Fine Tuning
// Close Variables
cslow=emaout + (crsi>80 ? log(crsi-80) : 0) + (psar<low ? 0.5 : 0)
cneutral=wmaout - (crsi<15 ? log(crsi) : 0) - (psar>high ? 0.5 : 0)
cfast= hullout

//Entry variables
eslow=emaout
eneutral=wmaout
efast= hullout
elonglag=0.1
eshortlag=0.1

// Plot
plot(eslow, title="slow", color=green, linewidth=4)
plot(eneutral, title="neutral", color=orange)
plot(efast, title="fast", color=red)
// Order placing

if time>timestamp(2017, 01, 01, 01, 01)
    closelong =  cfast<=cslow
    if (closelong)
        strategy.close("Long")
    closeshort = cfast>=cneutral
    if (closeshort)
        strategy.close("Short")
    longCondition = ((efast-elonglag >eslow and efast>eneutral or efast>bbbasis) and crsi<80 )
    if (longCondition)
        strategy.entry("Long",strategy.long)
    shortCondition = (efast <eslow and efast+eshortlag<eneutral)and crsi>15
    if (shortCondition)
        strategy.entry("Short",strategy.short)
