module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "arrow-parens": ["error", "always"],
    "func-names": ["error", "as-needed"],
    "space-before-function-paren": ["error", {
      "anonymous": "always",
      "named": "never",
      "asyncArrow": "never"
    }],
    "no-underscore-dangle": "off",
    "strict": 0,
    "object-curly-newline": [
      "error", { "multiline": true }
    ],
    "no-plusplus": "off"
  },
  "env": {
    "mocha": true,
    "node": true
  },
};
