const { express } = require('./boot');

const { PORT } = process.env;

const port = PORT || 8097;

const app = express();
app.listen(port, (err) => {
  if (!err) console.log(`app.boot :: Server running at port ${port}.`);
});
