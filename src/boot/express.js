const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const strategiesRoutes = require('../routes/strategies');

module.exports = () => {
  const app = express();
  app.use(cors());
  app.use(bodyParser.json()); // application/json
  app.use(bodyParser.urlencoded({ extended: true })); // application/x-www-form-urlencoded

  app.use('/strategies', strategiesRoutes);

  return app;
};
