// SMA
// The sma function returns the moving average, that is the sum of last y values of x, divided by y.

// PINE FUNCTION
// pine_sma(x, y) =>
//     sum = 0.0
//     for i = 0 to y - 1
//         sum := sum + x[i] / y
//     sum
const utils = require('../utils');

module.exports = (candles, length) => {
  const crop = utils.cropArray(candles, length);
  const sum = crop.reduce((acc, candle) => acc + candle.close, 0);
  return sum / length;
};
