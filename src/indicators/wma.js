/* WMA
 * The wma function returns weighted moving average of x for y bars back.
 * In wma weighting factors decrease in arithmetical progression.
*/

// PINE CALCULATION
// pine_wma(x, y) =>
//     norm = 0.0
//     sum = 0.0
//     for i = 0 to y - 1
//         weight = (y - i) * y
//         norm := norm + weight
//         sum := sum + x[i] * weight
//     sum / norm
const utils = require('../utils');

module.exports = (candles, length) => {
  const crop = utils.cropArray(candles, length);
  const dataset = typeof crop[0] === 'number' ? crop.map((c) => ({ close: c })) : crop;

  const values = dataset.reduce((acc, candle, index) => {
    const weight = length - index;
    acc.norm += weight;
    acc.sum += candle.close * weight;
    return acc;
  }, { norm: 0, sum: 0 });

  return values.sum / values.norm;
};
