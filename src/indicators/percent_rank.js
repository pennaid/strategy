// Percent Rank
// Percent rank is the percents of how many previous values was
// less than or equal to the current value of given series.
const utils = require('../utils');

// @return number between 0 - 100
module.exports = (candles, length) => {
  const crop = utils.cropArray(candles, length);
  if (crop.length === 1) return 0;

  const current = crop[0].close;
  const count = crop.reduce((acc, candle, index) => {
    if (index === 0) return acc;
    return current >= candle.close ? acc + 1 : acc;
  }, 0);

  return (count / (crop.length - 1)) * 100;
};
