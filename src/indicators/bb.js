const sma = require('./sma');
const utils = require('../utils');

module.exports = (candles, length) => {
  const len = length || candles.length;
  const croppedPrices = utils.cropArray(candles, len);

  return sma(croppedPrices, len);
};
