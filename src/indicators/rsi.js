// RSI
// Relative strength index. It is calculated based on rma's of upward and downward change of x.
const utils = require('../utils');

const calculateSmoothRS = ({ gains, losses, period, gain = 0, loss = 0, position = 0 }) => {
  const { length } = gains;

  const cropGains = gains.slice(length - period - position, length - position);
  const cropLosses = losses.slice(length - period - position, length - position);

  const avgGain = cropGains.reduce((acc, value) => acc + (value / period), 0);
  const avgLoss = cropLosses.reduce((acc, value) => acc + (value / period), 0);

  const mult = period - 1 || 1;

  const smoothedGain = ((avgGain * mult) + gain) / period;
  const smoothedLoss = (((avgLoss * mult) + loss) / period);

  const ratio = smoothedGain / smoothedLoss;

  return ratio === Infinity ? 0 : (isNaN(ratio) ? 1 : ratio); // eslint-disable-line
};

module.exports = (candles, period = 3, length) => {
  if (period < 1) throw Error('RSI: use period greater than 1');
  const crop = utils.cropArray(candles, length);
  const dataset = typeof crop[0] === 'number' ? crop.map((c) => ({ close: c })) : crop;

  const values = dataset.reduce((acc, candle, index) => {
    if (!dataset[index + 1]) return acc;
    const prevValue = dataset[index + 1].close;
    acc.gains.push(Math.max(candle.close - prevValue, 0));
    acc.losses.push(Math.max(prevValue - candle.close, 0));
    return acc;
  }, { gains: [], losses: [] });

  const first = calculateSmoothRS({ gains: values.gains, losses: values.losses, period });
  const rs = [first];

  let position = 0;
  for (let i = crop.length - period - 2; i >= 0; i--) {
    const gain = values.gains[i];
    const loss = values.losses[i];
    const smooth = calculateSmoothRS({
      gains: values.gains,
      losses: values.losses,
      gain,
      loss,
      period,
      position,
    });
    rs.unshift(smooth);
    position += 1;
  }

  return 100 - (100 / (1 + rs[0]));
};
