const utils = require('../utils');

function getSAR({ prev, antePrev }) {
  const { prevSAR, prevAF, prevEP, prevTrend, prevHigh, prevLow } = prev;
  const { antePrevLow, antePrevHigh, antePrevTrend } = antePrev;

  const calc = prevSAR + (prevAF * (prevEP - prevSAR));
  if (prevTrend === antePrevTrend) {
    if (prevTrend === 'up') {
      const min = Math.min(antePrevLow, prevLow);
      return calc < min ? calc : min;
    }
    const max = Math.max(antePrevHigh, prevHigh);
    return calc > max ? calc : max;
  }
  return prevEP;
}

function getAcclerationFactor({ trend, prevTrend, ep, prevEP, prevAF, maxAF, step }) {
  if (trend === prevTrend) {
    if (trend === 'up') {
      if (ep > prevEP) return prevAF < maxAF ? prevAF + step : prevAF;
    }
    if (trend === 'down') {
      if (ep < prevEP) return prevAF < maxAF ? prevAF + step : prevAF;
    }
    return prevAF;
  }
  return step;
}

function getExtremePrice({ high, low, prevEP, prevTrend }) {
  if (prevTrend === 'up') return high > prevEP ? high : prevEP;
  return low < prevEP ? low : prevEP;
}

function getTrend({ high, low, sar, prevTrend }) {
  if (prevTrend === 'up') return low > sar ? 'up' : 'down';
  return high < sar ? 'down' : 'up';
}

// originalCandles has the newest values at the beginning,
// They are inverted for ease of use
module.exports = (originalCandles, step = 0.02, maximum = 0.2) => {
  const candles = utils.invertArray(originalCandles);

  const firstTrend = 'up';

  const initialArray = [{
    af: step,
    trend: firstTrend,
  }, {
    sar: Math.min(candles[0].high, candles[1].high, candles[0].low, candles[1].low),
    af: step,
    ep: Math.max(candles[0].high, candles[1].high, candles[0].low, candles[1].low),
    trend: getTrend({ high: candles[0].high, low: candles[0].low, prevTrend: firstTrend }),
  }];

  const sarArray = candles.reduce((acc, candle, index) => {
    if (index <= 1) return acc;
    // acc[index - 1] is the prev, since we start with 2 initial values in acc
    const { sar: prevSAR, af: prevAF, ep: prevEP, trend: prevTrend } = acc[index - 1];
    const value = {};

    value.sar = getSAR({
      prev: {
        prevSAR,
        prevAF,
        prevEP,
        prevTrend,
        prevHigh: candles[index - 1].high,
        prevLow: candles[index - 1].low,
      },
      antePrev: {
        antePrevHigh: candles[index - 2].high,
        antePrevLow: candles[index - 2].low,
        antePrevTrend: acc[index - 2].trend, // acc[index - 2] is the antePrev
      },
    });

    value.trend = getTrend({
      prevTrend,
      sar: value.sar,
      high: candle.high,
      low: candle.low,
    });

    value.ep = getExtremePrice({
      prevEP,
      prevTrend,
      high: candle.high,
      low: candle.low,
    });

    value.af = getAcclerationFactor({
      trend: value.trend,
      ep: value.ep,
      prevTrend,
      prevEP,
      prevAF,
      maxAF: maximum,
      step,
    });

    acc.push(value);
    return acc;
  }, initialArray);

  sarArray.shift();

  return sarArray.reduce((acc, obj) => {
    acc.unshift(obj.sar);
    return acc;
  }, []);
};
