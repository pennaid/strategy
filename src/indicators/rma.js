// // RMA
// // Moving average used in RSI.
// // It is the exponentially weighted moving average with alpha = length - 1.
// const utils = require('../utils');
//
// module.exports = (prices, length) => {
//   const crop = utils.cropArray(prices, length);
//
//   return crop.reduce((acc, price, index) => {
//     const prevPrice = acc[index - 1] || 0;
//     const value = (price * length) + ((1 - length) * prevPrice);
//     acc.push(value);
//     return acc;
//   }, []);
// };
//
// //
// // pine_rma(x, y) =>
// //  alpha = y
// //     sum = 0.0
// //     sum := alpha * x + (1 - alpha) * nz(sum[1])
