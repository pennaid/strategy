const wma = require('./wma');

// hullout = wma(2*wma(hullsrc, hulllength/2)-wma(hullsrc, hulllength), round(sqrt(hulllength)))

module.exports = (candles, length = 1) => {
  const hullSeries = candles.reduce((acc, candle, index) => {
    const split = candles.slice(index, length + index);

    if (split.length < length) return acc;
    acc.push((2 * wma(split, Math.floor(length / 2))) - wma(split, length));

    return acc;
  }, []);

  const hullLength = Math.floor(Math.sqrt(length));
  return wma(hullSeries, hullLength);
};
