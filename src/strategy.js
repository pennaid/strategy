const wma = require('./indicators/wma');
const sma = require('./indicators/sma');
const hma = require('./indicators/hma');
const bb = require('./indicators/bb');
const rsi = require('./indicators/rsi');
const sar = require('./indicators/sar');
const percentRank = require('./indicators/percent_rank');
const utils = require('./utils');

// candles[100]
// Newest values comes first
module.exports = function strategy(params) {
  try {
    const {
      candles,
      wmaLength = 12,
      emaLength = 32,
      hullLength = 9,
      bbMult = 2,
      bbLength = 20,
    } = params;
console.log(candles.length);
    // Error or missing params check
    let error;
    if (bbMult < 0.001 || bbMult > 50) error = `bbMult should be more than 0.001 and less than 50. Sent value was ${bbMult}`;
    if (!candles) error = 'Candles must be setted';
    if (error) return { success: false, error };

    // Calculate indicators
    const wmaOut = wma(candles, wmaLength);
    const emaOut = sma(candles, emaLength);
    const hullOut = hma(candles, hullLength);
    const bbBasis = bb(candles, bbLength, bbMult);
    const rsiOut = rsi(candles, 3);
    const updownRsi = rsi(utils.updown(candles), 2);

    const pcRank = percentRank(utils.rateOfChange(candles, 1), 100);
    const crsi = utils.average(rsiOut, updownRsi, pcRank);

    const psar = sar(candles, 0.02, 0.2)[0];

    // Fine Tuning
    // Close Variables
    const cSlow =
      emaOut + (crsi > 80 ? Math.log(crsi - 80) : 0) + (psar < candles[0].low ? 0.5 : 0);
    const cNeutral =
      wmaOut - (crsi < 15 ? Math.log(crsi) : 0) - (psar > candles[0].high ? 0.5 : 0);
    const cFast = hullOut;

    // Entry variables
    const eSlow = emaOut;
    const eNeutral = wmaOut;
    const eFast = hullOut;
    const eLongLag = 0.1;
    const eShortLag = 0.1;

    const closeLong = cFast <= cSlow;
    const closeShort = cFast >= cNeutral;
    const entryLong = ((eFast - eLongLag > eSlow && eFast > eNeutral) || eFast > bbBasis) && crsi < 80; // eslint-disable-line
    const entryShort = (eFast < eSlow && eFast + eShortLag < eNeutral) && crsi > 15;

    const entry = entryLong ? 'long' : entryShort ? 'short' : null; // eslint-disable-line no-nested-ternary
    const close = closeLong ? 'long' : closeShort ? 'short' : null; // eslint-disable-line no-nested-ternary

    // Order placing
    return {
      entry,
      close,
    };
  } catch (e) {
    console.log(e);
    return { success: false, error: e.message, stack: e.stack };
  }
};
