/* eslint-disable no-nested-ternary */
function cropArray(array, length) {
  if (!length) return array;
  if (length > array.length) {
    console.log('utils.cropArray :: Length is greater than array length');
    return array;
  }
  return length < array.length ? array.slice(0, length) : array;
}

function invertArray(array) {
  return array.reduce((acc, value) => {
    acc.unshift(value);
    return acc;
  }, []);
}

function updown(candles) {
  return candles.reduce((acc, candle, index) => {
    const prevValue = (candles[index + 1] && candles[index + 1].close) || 0;
    const prevAcc = acc[index + 1] || 0; // TODO acc[index + 1] is always undefined
    let ud;
    if (candle.close === prevValue) {
      ud = 0;
    } else if (candle.close > prevValue) {
      ud = prevAcc <= 0 ? 1 : (prevAcc + 1);
    } else {
      ud = prevAcc >= 0 ? -1 : (prevAcc - 1);
    }
    acc.push(ud);
    return acc;
  }, []);
}

// @return array of numbers between 0 - 100
function rateOfChange(candles, period, length) {
  const crop = cropArray(candles, length);
  return crop.reduce((acc, candle, index) => {
    if (index + period >= crop.length) return acc;
    const back = crop[index + period].close;
    const delta = (candle.close - back) / back;
    acc.push(delta * 100);
    return acc;
  }, []);
}

function average(...args) {
  const sum = args.reduce((avg, arg) => avg + arg, 0);
  return sum / arguments.length;
}

// NOT USED NOW
// function lowest(series, length) {
//   const crop = cropArray(series, length);
//   return crop.reduce((acc, current) => {
//     if (current < acc) return current;
//     return acc;
//   });
// }
//
// function highest(series, length) {
//   const crop = cropArray(series, length);
//   return crop.reduce((acc, current) => {
//     if (current > acc) return current;
//     return acc;
//   });
// }
//
// function donchian(high, low, length) {
//   const lowValues = cropArray(low, length);
//   const highValues = cropArray(high, length);
//   return average(lowest(lowValues), highest(highValues));
// }

module.exports = {
  cropArray,
  updown,
  rateOfChange,
  average,
  invertArray,
};
