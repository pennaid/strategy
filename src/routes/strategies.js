const { Router } = require('express');
const strategy = require('../strategy');

const router = Router();

const AUTH_KEY = 'hBDyTsPewtHBpjFtESg5';

router.post('/v1', (req, res) => {
  const { headers } = req;
  if (headers.authentication !== AUTH_KEY) return res.status(401).send({ success: false, error: 'Wrong authentication.' });

  const advice = strategy(req.body);

  const status = advice.error ? 400 : 200;
  return res.status(status).send(advice);
});

module.exports = router;
